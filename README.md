# renamer

A batch file renamer written in flutter (dart).

todo:
- ~~Duplicate name check~~.(Done.)
- ~~Convert, including case convert, Chinese simp/trad/pinyin convert, Latin/Cyrillic script transliteration~~.(Done.)
- ~~Incremental renaming: for example, RenamerFile-1, RenamerFile-2, RenamerFile-3, RenamerFile-4, ...~~.(Done.)
- Rules re-editing.

# Screenshots
## Desktop
| ![Desktop-0](/screenshots/Desktop-0.png?raw=true) | ![Desktop-1](/screenshots/Desktop-1.png?raw=true) |
|:--------------------------------------------------|:--------------------------------------------------|
| ![Desktop-2](/screenshots/Desktop-2.png?raw=true) | ![Desktop-3](/screenshots/Desktop-3.png?raw=true) |

## Phone
| ![Phone-0](/screenshots/Phone-0.jpg?raw=true) | ![Phone-1](/screenshots/Phone-1.jpg?raw=true) | ![Phone-2](/screenshots/Phone-2.jpg?raw=true) | ![Phone-3](/screenshots/Phone-3.jpg?raw=true) |
|:----------------------------------------------|:----------------------------------------------|:----------------------------------------------|:----------------------------------------------|

## Seven-inch Tablet
| ![Seven-inch_Tablet-0](/screenshots/Seven-inch_Tablet-0.png?raw=true) | ![Seven-inch_Tablet-1](/screenshots/Seven-inch_Tablet-1.png?raw=true) |
|:----------------------------------------------------------------------|:----------------------------------------------------------------------|
| ![Seven-inch_Tablet-2](/screenshots/Seven-inch_Tablet-2.png?raw=true) | ![Seven-inch_Tablet-3](/screenshots/Seven-inch_Tablet-3.png?raw=true) |

## Ten-inch Tablet
| ![Ten-inch_Tablet-0](/screenshots/Ten-inch_Tablet-0.png?raw=true) | ![Ten-inch_Tablet-1](/screenshots/Ten-inch_Tablet-1.png?raw=true) |
|:------------------------------------------------------------------|:------------------------------------------------------------------|
| ![Ten-inch_Tablet-2](/screenshots/Ten-inch_Tablet-2.png?raw=true) | ![Ten-inch_Tablet-3](/screenshots/Ten-inch_Tablet-3.png?raw=true) |
